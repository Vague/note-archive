#####################################################
# site layout
# TODOS
# functions
# arg handling (lazy)

#####################################################
# TODOS 
# ideas: empty/few line file finder
# can i output like mermaid graph?
# dead file link finder
# also show tag analytics - which tag tree is most edited vs longest
# getopts?
# func to add ln -s this script to PATH so i dont have to. --install

#####################################################
# functions 

show_high_level_tags(){
	ls * | cut -d . -f 1 | sort -u
}

backup_function(){
	if [ -z "$1" ] ; then
		echo provide output path
		exit 1	
	fi
	if [ ! -d "$1" ] ; then
		echo the output location doesnt exist!
		exit 1	
	fi
	echo backup function $@
	set -x 
	tar caf "$1"/notes_backup_$(date +%Y%m%d).tar.gz  *
	set +x
	
}

usage_message(){
cat <<EOF
Usage: $0 [command]...
Note editing facility.

Usage: $0 			- show last edited files
Usage: $0 mknode|-m node[/] 	- create a node and open it in vim
Usage: $0 mknode|-mr node[/] 	- create a node and echo created file name for :r! bash utility.sh ...
Usage: $0 --list-recent | -l  	- show last edited files
Usage: $0 todo  | -t  		- find all TODOs
Usage: $0 rename src dest	- rename and relink all occurances of a file
Usage: $0 --help | -h 		- display this message
Usage: $0 showlinks | -sl 	- find all files linking to other files
Usage: $0 backup path/to/output	- create a backup of the note dir at given location/folder
Usage: $0 hlt 			- show high level tags (ls * | cut -d . -f1 | sort -u)
EOF
}

last_edited_files(){
	echo last edited files:
	ls -alt | head
}

renamer(){
	src=$1
	tgt=$2

	test -z "$src" -o \
	-z "$tgt"  -o \
	! -e "$src" && { 
		echo '$1' is existing file, '$2' is what it will be relinked and moved to ; exit 1 ; 
	}

	echo relinking
	grep -r $src | grep -v .git | while read line ; do
		affected_file=$(echo $line | cut -d : -f1 )
		echo affected_file, $line
		sed -e 's/'$src/$tgt/ $affected_file -i
	done
	mv -v "$src" "$tgt"
}


mknode(){
	# TODO use string slices instead of grep, ie test last char = /
	echo $1 | grep /$ >/dev/null &&  {
		nodename=$(echo $1 | sed -e 's_/$__')
		mkdir -p "$nodename-$(date +%Y%m%d)"
		echo "$nodename-$(date +%Y%m%d)"
	} || { 
# we may not want to create files immediately. disable if it becomes annoying
		nodename="$1-$(date +%Y%m%d).mkd"
		[ ! -e "$nodename" ] && echo created $(date) >> "$nodename"
	}
	echo $nodename
}

#####################################################
# arg handling (this could be getopts. todo?)

[ -z $1 ] && {
	last_edited_files
	exit 1
} 

[ $1 = '-h' -o $1 = '--help' ] && { usage_message ; exit ; }

[ "$1" = "--rename" -o "$1" = '-r' ] && {
	shift
	renamer $@
	exit $?
}

if [ "$1" = '--list-recent' -o "$1" = '-l' ] ; then
	last_edited_files
	exit $?
fi


# use in vim with :r! bash utility.sh -mr mynewnode and then gf to new node. quite nice
if [ $1 = mknode_dontvim -o $1 = '-mr' ] ; then
	#mknode is what you go to, to make files or folders in the archive. it makes both with the -yyyymmdd[.mkd[/]] format so everything is consistent
	shift
	# mknote needs to be mknode and be capable of making folders as well as text files
	if [ -z "$1" ] ; then
		echo bad args
		exit
	fi

	fn=$(mknode "$1")
	echo $fn
fi

if [ $1 = mknode -o $1 = '-m' ] ; then
	#mknode is what you go to, to make files or folders in the archive. it makes both with the -yyyymmdd[.mkd[/]] format so everything is consistent
	shift
	# mknote needs to be mknode and be capable of making folders as well as text files
	if [ -z "$1" ] ; then
		echo bad args
		exit
	fi

	fn=$(mknode "$1")
	vim "$fn"
fi

if [ $1 = 'showlinks' -o $1 = '-sl' ] ; then
	for a in *.mkd ; do 
		grep -r "$a" | sed -e 's/:.*/: ---> '$a/  
	done  | col
	exit $?
fi

if [ $1 = 'todo' -o "$1" = '-t' ] ; then
	grep TODO -r | grep -v -e $0 -e .git
	exit $?
fi

if [ $1 = 'backup' -o "$1" = '-b' ] ; then
	shift
	backup_function "$@"
	exit $?
fi

if [ $1 = 'hlt'  ] ; then
	shift
	show_high_level_tags "$@"
	exit $?
fi
