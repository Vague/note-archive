# note-archive

Welcome to the note archive. 

We post recipes here. Feel free to make a pull request. 

All the files are markdown, which should allow this to be convertable to a website for hosting later. 
utility.sh is where I plan to make tools for things like renaming, graph analysis and so on. 
I've been editing everything in vim. 

# idea behind filenames
Having all the files in one folder with dots instead of folder layouts has interesting benefits. It works well with tab completion and searching.
https://wiki.dendron.so/notes/f3a41725-c5e5-4851-a6ed-5f541054d409/
